
import java.io.IOException;
import java.util.*;
import java.io.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;

import org.apache.hadoop.fs.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.mapred.Reducer;
import java.util.logging.Logger;

@SuppressWarnings("deprecation")
public class H_KMeans2D {
	private static String OUT ;
	private static String IN ;
	
	private static String JOB_NAME = "H_KMeans2D";
	private static String SPLIT_BY = "\t|,| ";
	private static String hadoop_outFileName = "/part-00000";
	
	private static int counter ;
	private static int kCount ; //default
	private static int MaxIterations ; //default 
	private static Logger logger = Logger.getAnonymousLogger();
	
	 
	public static class Map extends MapReduceBase implements
			Mapper<LongWritable, Text, Text, Text> {
		
		private static List<String> kCentroids = new ArrayList<String>();
		@Override
		public void configure(JobConf job) {
			try {
				 
				Path[] cacheFiles = DistributedCache.getLocalCacheFiles(job);
				if (cacheFiles != null && cacheFiles.length > 0) {
					
					
					String line;
					BufferedReader cacheReader = new BufferedReader(
							new FileReader(cacheFiles[0].toString()));
					try {
						 
						line = cacheReader.readLine();
						while (line!= null ) {
							
							String[] temp = line.split(SPLIT_BY);
							kCentroids.add(temp[0] + "," + temp[1]);  
	 		
							line = cacheReader.readLine();
						}
					} finally {

						cacheReader.close();
					}
					
				}
			} catch (IOException e) {
				 //skip
			}
		}


		
		public static double distance1(Double p, Double q){
			
			return Math.abs(p-q);
		} //distance1
		
		public static double distance(Double x1, Double y1, Double x2, Double y2){
			double xx = x1-x2;
			double yy = y1-y2;
			return Math.sqrt(xx*xx+yy*yy);
		}
		
	 
		@Override
		public void map(LongWritable key, Text value,
				OutputCollector<Text, Text> output,
				Reporter reporter) throws IOException {
			
			String line = value.toString();
			String[] temp = line.split(SPLIT_BY);
			double pointx = Double.parseDouble(temp[0]);
			double pointy = Double.parseDouble(temp[1]);
			String point = pointx +","+pointy;
			
			double min1, min2 = Double.MAX_VALUE; //distance
		 
			String[] coords = kCentroids.get(0).split(SPLIT_BY);
			String nearest_center = coords[0] +","+ coords[1];
		 
			for (String c : kCentroids) {
				String[] coords2 = c.split(SPLIT_BY);
				double cx = Double.parseDouble(coords2[0]);
				double cy = Double.parseDouble(coords2[1]);
				min1 = distance(cx, cy, pointx, pointy);
				if (min1 < min2) {
					nearest_center = cx+","+cy;
					min2 = min1;
				}
			}
			// Emit 
	 
			output.collect(new Text(""+nearest_center), new Text(""+point));
		}
	}

	public static class Reduce extends MapReduceBase implements
			Reducer<Text, Text, Text, IntWritable> {


		@Override
		public void reduce(Text key, Iterator<Text> values,
				OutputCollector<Text, IntWritable> output, Reporter reporter)
				throws IOException {
			 

			
			int members = 0;
			double sumx = 0.0;
			double sumy = 0.0;
			while (values.hasNext()) {
				String s = values.next().toString();
				String[] coords = s.split(SPLIT_BY);
				
				double pointx = Double.parseDouble(coords[0]);
				double pointy = Double.parseDouble(coords[1]);
				sumx += pointx;
				sumy += pointy;
  
				members +=1;	
			}//while
 
			Double newX = sumx / members;
			Double newY = sumy / members;
			String newPoint = newX + "," + newY; 
			
 
			 
			output.collect(new Text(""+newPoint), new IntWritable(members));
		}
	}

	
	public static void createCent(String hadoopInputPath){
		
		HashSet<String> strands = new HashSet<String>();
		try{
			FileSystem fs = FileSystem.get(new Configuration());
			FileStatus[] status = fs.listStatus( new Path( hadoopInputPath ) );
			Path path = status[0].getPath(); 

			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(path)));
			
			String line;
			int cent_count = kCount;
			while(cent_count >0 && (line=br.readLine())!=null){
				String[] temp = line.split(SPLIT_BY);
				String key = temp[0]+","+temp[1];
			 
				if(!strands.contains(key)){
					strands.add(key);
					cent_count -= 1;
				}
			}
			br.close();
			
			
		}
		catch (Exception e){
			
		} 
		
		try{
		    PrintWriter writer = new PrintWriter("CENTROIDS.txt", "UTF-8");

		    for (String strand:strands){
		    	writer.println(strand);
		    }
		    writer.close();
		    
		} catch (Exception e) {
		   // do something
		}
		
		//using threads.sleep to allow time for the file
		//to be copied to other hadoop clusters.
		//I found out that without sleep,
		//most of the time, the file doesn't get copied properly
		
		try{
			System.out.println("del start");
		    Runtime.getRuntime().exec(
					new String[] {"/bin/sh", "-c", "hadoop dfs -rmr /user/hadoop/tempor*"}, 
									null);
		    Thread.sleep(1000);
		    System.out.println("del done");
		}
		catch(Exception e){
			
		}
	
		try{
			System.out.println("mkdir start");
		    Runtime.getRuntime().exec(
					new String[] {"/bin/sh", "-c", "hadoop dfs -mkdir /user/hadoop/tempora"}, 
									null);
		    System.out.println("mkdir done");
		    Thread.sleep(1000);
		}
		catch(Exception e){
			
		}
		
		try{
			
			System.out.println("copy start");
		    Runtime.getRuntime().exec(
					new String[] {"/bin/sh", "-c", "hadoop dfs -copyFromLocal CENTROIDS.txt /user/hadoop/tempora"}, 
									null);
		    System.out.println("copy done");
		}
		catch(Exception e){
			 
		}
		
 
		
	}
	
 
	public static void main(String[] args) throws Exception {
		IN = args[0];
		OUT = args[1];
 
	 
		kCount = Integer.parseInt(args[3]);
		MaxIterations = Integer.parseInt(args[4]);
		
		String input = IN;
 
		
		//initialize static variable counter
		counter =1;
		String output = OUT + counter;
		counter +=1;
		
		String re_input = output;
		
		
		createCent(IN);
		Thread.sleep(500);
		 
		final long startTime = System.nanoTime();
		int iteration = 0;
		boolean isdone = false;
		while (isdone == false) {
			JobConf conf = new JobConf(H_KMeans2D.class);
			if (iteration == 0) {
				Path hdfsPath = new Path("/user/hadoop/tempora/CENTROIDS.txt");

				DistributedCache.addCacheFile(hdfsPath.toUri(), conf);
			} else {
				
				Path hdfsPath = new Path(re_input + hadoop_outFileName); 
				DistributedCache.addCacheFile(hdfsPath.toUri(), conf);
			}

			conf.setJobName(JOB_NAME);
			
			
			conf.setNumReduceTasks(1);
			
			conf.setMapOutputKeyClass(Text.class);
			conf.setMapOutputValueClass(Text.class);
			conf.setOutputKeyClass(Text.class);
			
			conf.setOutputValueClass(IntWritable.class);
			
			conf.setMapperClass(Map.class);
			conf.setReducerClass(Reduce.class);
			conf.setInputFormat(TextInputFormat.class);
			conf.setOutputFormat(TextOutputFormat.class);
			
			 
			FileInputFormat.setInputPaths(conf,
					new Path(input ));
			FileOutputFormat.setOutputPath(conf, new Path(output));

			JobClient.runJob(conf);

			Path ofile = new Path(output + hadoop_outFileName);
			FileSystem fs = FileSystem.get(new Configuration());
			BufferedReader br = new BufferedReader(new InputStreamReader(
					fs.open(ofile)));
			List<String> newCentroids = new ArrayList<String>();
			

			
			if(iteration>0){

					if (  iteration>=MaxIterations) {
						isdone = true;
					} else {
						isdone = false;
						break;
					}
	
			} //if
			
			if (MaxIterations==0) isdone=true;
			
			++iteration;
			re_input = output; 
			
			output = OUT + counter;
			counter +=1;
		}
		double duration = System.nanoTime() - startTime;
		duration = duration /1000000000; // into seconds
		
		System.out.println("\n 2D k-means execution time = " + duration);
	}
}