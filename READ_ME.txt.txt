<Aayush Karki>

For both H_HMeans2D.java and H_DNA.java assumed that /user/hadoop path exists.

Both programs read the file from the given input folder and create a file consisting of initial list of centroids called CENTROIDS.txt.

Both programs copies CENTROIDS.txt into the hdfs at /user/hadoop/tempora.
"tempora" is a folder in hdfs created by the program at runtime.

The runtime of H_Means2D.java does not match what I have in the analysis report. After I was done making the report,
I tried to optimized H_Means2D.java, and realized that removing a check for conversion and simply relying on the iteration
improves performance significantly.

The output of the both program resides in the supplied output folder. if "/user/hadoop/a/b/out_put/" is the output folder,
then the final list of centroids is at /user../../out_put/output<num> where num is the largest numbered folder inside ../../out_put/.
Go to the largest numbered one, and the centroids are stored in part-00000


And finally:
H_DNA.java works correctly however there is one problem. The strandlength is not parsed correctly for reasons I could bot figure out.

Simply changing:

	private static  int kCount = 10 ;  //num of clusters
	private  static int MaxIterations =10;   //number of iterations
	private static  int STRAND_LEN = 20 ;  // length of strand

 the above attributes in H_DNA.java to appropriate parameters ensures correct execution.